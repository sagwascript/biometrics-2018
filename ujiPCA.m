load data_training
load mat1DimDataTesting
load dataMat
load data_testing
orangKe = 10;
poseKe = 1;
jumlahPose = size(data_training, 2);

%% proses PCA
outputPCA = myPCA(dataMat)
%% hitung bobot data uji
dataUji = mat1DimDataTesting{orangKe, poseKe};
bobotUji = dataUji * outputPCA.proyeksi;

outputP = myManhattan(outputPCA.bobot, bobotUji, jumlahPose);
yangDiuji = data_testing{orangKe, poseKe};
hasilUji = data_training{outputP.orangKe, outputP.poseKe};
imshow([yangDiuji hasilUji]);

if outputP.orangKe == orangKe
   msgbox('Benar');
else
   msgbox('Salah');
end