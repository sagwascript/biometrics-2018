function [OutputFile] = loadimage(jumlahKelas, jumlahPose)
    citraTersimpan = 0;
    wb = waitbar(0, 'Sedang memproses');
    for iterasiKelas = 1:jumlahKelas
        imgPath = ['D:\Kuliah\Semester VI\Biometrika\YALE\s' num2str(iterasiKelas) '\'];
        for iterasiPose = 1:jumlahPose
            citraTersimpan = citraTersimpan + 1;
            OutputFile.ORLFile{iterasiKelas,iterasiPose} = imread([imgPath num2str(iterasiPose) '.pgm']);
            waitbar(citraTersimpan/(jumlahKelas * jumlahPose));
        end
    end
    OutputFile.jumlahKelas = jumlahKelas;
    OutputFile.jumlahPose = jumlahPose;
    close(wb);
