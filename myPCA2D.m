function hasil = myPCA2D(dataTraining)
% mendapatkan banyak kelas dan pose dari data training
[nkelas npose] = size(dataTraining);
%% mencari mean
rata2 = 0;
% perulangan ke seluruh kelas dan kolom dari data training
for iterKelas = 1:nkelas
   for iterPose = 1:npose
       % menambahkan nilai rata2 dengan semua matriks citra didalam data training
       rata2 = rata2 + double(dataTraining{iterKelas, iterPose});
   end
end
% mendapatkan nilai rata2
rata2 = rata2 / (nkelas * npose);
%% menghitung zero mean

C = 0;
for iterKelas = 1:nkelas
   for iterPose = 1:npose
       % menghitung zero mean tiap citra
       zeroMean{iterKelas, iterPose} = double(dataTraining{iterKelas, iterPose}) - rata2;
       % mengambil nilai zero mean
       zeroM = zeroMean{iterKelas, iterPose};
       %% menghitung covariance
       % menghitung dan menyimpan hasil perkalian transpose matriks zero mean dan matriks zero mean
       kovarian{iterKelas, iterPose} = transpose(zeroM) * zeroM;
       % menambahkan nilai pada C dengan hasil perhitungan kovarian
       C = C + kovarian{iterKelas, iterPose};
   end
end
%% menghitung eigenvalue dan eigenvector dan mengurutkan secara decreasing eigenvaluenya diikuti dengan kolom eigenvector
[eigvector eigvalue] = svd(C);
% menyimpan eigenvektor kedalam hasil.proyeksi
hasil.proyeksi = eigvector;

%% menghitung bobot
% perulangan pada tiap citra didalam data training
for iterKelas = 1:nkelas
   for iterPose = 1:npose
       % menghitung bobot tiap citra dan menyimpan nilainya didalam cell array
       bobot{iterKelas, iterPose} = double(dataTraining{iterKelas, iterPose}) * eigvector;
   end
end
% menyimpan eigenvektor kedalam hasil.bobot
hasil.bobot = bobot