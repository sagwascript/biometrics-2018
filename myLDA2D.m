function hasil = myLDA2D(dataTraining)
jumlahPose = 3;
[barisDataLDA kolomDataLDA] = size(dataTraining);

%% menghitung rata-rata tiap kelas
counter = 1;
counterKelas = 1;
matTiapKelas = [];
imgDalamKelas = 0;
for iterasiKelas=1:barisDataLDA
    jumlahImgTiapKelas = 0;    
    for iterasiPose=1:kolomDataLDA
       jumlahImgTiapKelas = jumlahImgTiapKelas + double(dataTraining{iterasiKelas, iterasiPose});
    end
    imgDalamKelas = imgDalamKelas + jumlahImgTiapKelas;
    dataTotalTiapKelas{iterasiKelas} = jumlahImgTiapKelas;
    meanTiapKelas{iterasiKelas} = jumlahImgTiapKelas / kolomDataLDA;
end

meanSemuaKelas = imgDalamKelas / (barisDataLDA * kolomDataLDA);
%% menghitung SB
SB = 0;
SW = 0;
for iterasiKelas = 1:barisDataLDA
    for iterasiPose = 1:kolomDataLDA
        %% menghitung SW
        rumus = double(dataTraining{iterasiKelas, iterasiPose})-meanSemuaKelas;
        hitung = transpose(rumus) * rumus;
        % IMPORTANT NOTE: SB otomatis ditransformasikan menjadi matriks 0
        % dengan ukuran jumlahkelas x jumlahpose
        SB = SB + hitung;
    end
    %% menghitung SW
    rumusSW = dataTotalTiapKelas{iterasiKelas}-meanTiapKelas{iterasiKelas};
    hitungSW = transpose(rumusSW) * rumusSW;
    SW = SW + hitungSW;
end

%% menghitung eigenvector dan eigenvalue input LDA
kovarian = SB/SW;
kovarian = kovarian + transpose(kovarian);
[eigvectorLDA eigvalueLDA] = svd(kovarian);
hasil.proyeksi = eigvectorLDA;

%% menghitung bobot
for iterKelas = 1:barisDataLDA
   for iterPose = 1:kolomDataLDA
       bobot{iterKelas, iterPose} = double(dataTraining{iterKelas, iterPose}) * eigvectorLDA;
   end
end
hasil.bobot = bobot