clear all
clc
load data_training
load mat1DimDataTesting
load YaleFile
load data_testing

%orangKe = 10;
%poseKe = 1;
jumlahPose = size(data_training, 2);
namaFile = ['hasilexp-' num2str(jumlahPose) '.xls']
hasilAkhir = [];

pose = 3;
[ndata nPoseData] = size(YaleFile);

fiturAwal = 10;
fiturAkhir = 12;
jumlahExp = fiturAkhir-fiturAwal;
wrap = zeros(jumlahExp, jumlahExp);

%% proses uji 
kFold = 5;
for iterKfold=1:kFold
    %% generate K-fold
    expIdx = randperm(nPoseData);
    trainIdx = expIdx(1,1:pose);
    testIdx = expIdx(1,pose+1:nPoseData);
    trainIdx = sort(trainIdx);
    testIdx = sort(testIdx);
    
    %% proses PCA
    data_training = YaleFile(:,trainIdx);
    data_testing = YaleFile(:,testIdx);
    outputPCA = myLDA2D(data_training);
    [jumlahOrangTesting jumlahPoseTesting] = size(data_testing);
    
    kesimpulan = [];
    
    %% variabel menampung tiap experiment
    expResult = [];
    xValidation = [];
    counter = 1;
    for fitur = fiturAwal:fiturAkhir
        h = waitbar(0, 'Proses');
        counter = 0;
        hasilAkhir = [];
        benar = 0;
        salah = 0;

        [nkelas npose] = size(outputPCA.bobot);
        for iterKelas = 1:nkelas
           for iterPose = 1:npose
               potongBobotTrain{iterKelas, iterPose} = outputPCA.bobot{iterKelas, iterPose}(:,1:fitur);
           end
        end

        detilKesimpulan = [];
        for jOrangTesting = 1:jumlahOrangTesting
            kumpulanHasil = [];
            benarSetiapKelas = 0;
            salahSetiapKelas = 0;
            detilKesimpulanTiapKelas = [];
            for jPoseTesting = 1:jumlahPoseTesting
                counter = counter + 1;
                potongBobotUji = double(data_testing{jOrangTesting, jPoseTesting}) * outputPCA.proyeksi(:,1:fitur);

                outputP = my2DEuclideanDistance(potongBobotTrain, potongBobotUji, jumlahPose);

                yangDiuji = data_testing{jOrangTesting, jPoseTesting};
                hasilUji = data_training{outputP.orangKe, outputP.poseKe};

                hasil = [yangDiuji hasilUji];
                kumpulanHasil = [kumpulanHasil; hasil];
                waitbar(counter/(jumlahOrangTesting*jumlahPoseTesting));
                if outputP.orangKe == jOrangTesting
                    benar = benar + 1;
                    benarSetiapKelas = benarSetiapKelas + 1;
                    detilKesimpulanTiapKelas = [detilKesimpulanTiapKelas; jOrangTesting jPoseTesting 1];
                else
                    salah = salah + 1;
                    salahSetiapKelas = salahSetiapKelas + 1;
                    detilKesimpulanTiapKelas = [detilKesimpulanTiapKelas; jOrangTesting jPoseTesting 0];
                end
            end
            detilKesimpulan = [detilKesimpulan; detilKesimpulanTiapKelas];
            namaSheet = ['jumlahfitur-' num2str(fitur)];
            hasilSementara = [jOrangTesting benarSetiapKelas salahSetiapKelas];
            hasilAkhir = [hasilAkhir; hasilSementara];
            namaFileImg = ['2DLDA-mod orangke-' num2str(jOrangTesting) '.jpg'];
            imwrite(kumpulanHasil, namaFileImg);
        end
        %% simpan excel tiap hasil akhir
        xlswrite(namaFile, hasilAkhir, namaSheet);
        %% simpan excel tiap detail percobaan
        xlswrite(namaFile, detilKesimpulan, ['Detail_percobaan_ke-' num2str(fitur)]);

        sementara = [fitur benar salah];
        hasilAkhir = [hasilAkhir; sementara];
        kesimpulan = [kesimpulan; sementara];
        close(h);
        
        prosentasiAkurasi = (benar/(benar+salah))*100;
        wrap(iterKfold, counter) = prosentasiAkurasi;
        expResult = [expResult; trainIdx fitur prosentasiAkurasi];
        save kesimpulan kesimpulan;
        xlswrite(namaFile, kesimpulan, 'Kesimpulan');
        counter = counter + 1;
    end
    %% simpan excel tiap detail percobaan
    xValidation = [xValidation; expResult];
    xlswrite(namaFile, xValidation, [num2str(iterKfold) '-fold']);
end

%% menampilkan bar
figure
%bar(kesimpulan(:, 1), kesimpulan(:, [2 3]));