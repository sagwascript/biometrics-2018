function outputP = my2DEuclideanDistance(bobotTraining, bobotTesting, jumlahPose)
% mendapatkan jumlah baris dan kolom pada bobot training
[jumlahBaris jumlahKolom] = size(bobotTraining);
% perulangan pada setiap citra didalam bobot training
for jBaris = 1:jumlahBaris
    for jKolom = 1:jumlahKolom
        % menghitung jarak berdasarkan rumus euclidean
        jarak = sqrt(sum(sum((bobotTesting-bobotTraining{jBaris, jKolom}).^2), 2));
        % menyimpan jarak pada array jaraknya
        jaraknya(jBaris,jKolom) = jarak; 
    end
end
% mendapatkan posisi kolom dengan nilai jarak terdekat
[nilaiMin kolom] = min(jaraknya, [], 2);
% mendapatkan posisi baris berdasarkan array jarak kolom terdekat
[nilaiMin baris] = min(nilaiMin);
% menyimpan posisi baris citra dengan jarak terdekat
outputP.orangKe = baris;
% menyimpan posisi kolom citra dengan jarak terdekat
outputP.poseKe = kolom;