clear all
clc

% memuat file mat dataset Yale
load YaleFile

% memanggil modul preprocess untuk mengubah seluruh dataset menjadi diagonal
preprocess(YaleFile)

% memuat data mat hasil generate modul preprocess
load diaDataMat

% menentukan jumlah pose yang akan digunakan sebagai data training
jumlahPose = 3;

% mendapatkan banyak data dan banyak pose dari file mat
[ndata nPoseData] = size(diaDataMat);
% menentukan banyak fitur paling sedikit yang digunakan dari hasil PCA
fiturAwal = 10;
% menentukan banyak fitur paling banyak yang digunakan dari hasil PCA
fiturAkhir = 15;
% menghitung jumlah eksperimen yang dilakukan
jumlahExp = fiturAkhir - fiturAwal;

%% prose kfolding
kFold = 5;
% menampung indeks data training tiap kfold
trainIdxAll = [];
% generate matriks 0 dengan ukuran jumlah ekperimen x banyak kfold
tiapKfold = zeros(jumlahExp, kFold);

% perulangan proses kfold
for iterKfold=1:kFold
    
    % mendefinisikan nama file
    namaFile = [num2str(iterKfold) '-diaPCA2D-hasilexp-' num2str(jumlahPose) '.xls'];
    %% generate K-fold
    % generate angka acak permutasi dari 1 hingga banyaknya pose data
    expIdx = randperm(nPoseData);
    % memotong array expIdx untuk dijadikan indeks data training
    trainIdx = expIdx(1,1:jumlahPose);
    % memotong array expIdx untuk dijadikan indeks data testing
    testIdx = expIdx(1,jumlahPose+1:nPoseData);
    
    % mengurutkan nilai didalam array
    trainIdx = sort(trainIdx);
    testIdx = sort(testIdx);
    
    % menyimpan indeks pose data training pada tiap kfold
    trainIdxAll = [trainIdxAll; trainIdx];
    
    %% proses PCA
    % menentukan data yang digunakan sebagai data training berdasarkan
    % trainIdx
    data_training = diaDataMat(:,trainIdx);
    % menentukan data yang digunakan sebagai data testing berdasarkan
    % trainIdx
    data_testing = diaDataMat(:,testIdx);
    % melakukan proses PCA dengan input data training
    outputPCA = myPCA2D(data_training);
    % mendapatkan banyak baris dan kolom dari cell array data testing
    [jumlahOrangTesting jumlahPoseTesting] = size(data_testing);
    
    %% variabel menampung tiap experiment
    % variabel untuk menampung hasil eksperimen tiap kfold
    expResult = [];
    % variabel untuk menampung persentasi pada tiap kfold
    xValidation = [];
    %% proses uji
    for fitur = fiturAwal:fiturAkhir
        % menampilkan waitbar
        h = waitbar(0, 'Proses');
        % mendefinisikan variabel counter untuk indikator waitbar
        counter = 0;
        % variabel untuk menampung hasil eksperimen
        hasilAkhir = [];
        % inisiasi variabel benar untuk menghitung citra yang cocok
        benar = 0;
        % inisiasi variabel salah untuk menghitung citra yang tidak cocok
        salah = 0;
        
        % mendapatkan banyak baris dan kolom dari hasil bobot data traing
        % pada proses PCA
        [nkelas npose] = size(outputPCA.bobot);
        
        % perulangan pada tiap baris dan kolom
        for iterKelas = 1:nkelas
            for iterPose = 1:npose
                % memotong seluruh bobot data training sesuai dengan iterasi fitur
                potongBobotTrain{iterKelas, iterPose} = outputPCA.bobot{iterKelas, iterPose}(:,1:fitur);
            end
        end
        
        % variabel untuk menyimpan tiap pencocokan citra
        detilKesimpulan = [];
        
        % perulangan pada tiap data testing
        for jOrangTesting = 1:jumlahOrangTesting
            kumpulanHasil = [];
            % mendefinisikan variabel untuk mencatat banyaknya citra yang
            % benar tiap kelas
            benarSetiapKelas = 0;
            salahSetiapKelas = 0;
            
            % variabel untuk menyimpan tiap pencocokan citra di tiap kelas
            detilKesimpulanTiapKelas = [];
            for jPoseTesting = 1:jumlahPoseTesting
                % melakukan incremen variabel counter
                counter = counter + 1;
                % untuk memotong hasil penghitungan bobot testing
                potongBobotUji = double(data_testing{jOrangTesting, jPoseTesting}) * outputPCA.proyeksi(:,1:fitur);
                
                % memanggil modul untuk menentukan citra dengan jarak
                % terdekat
                outputP = my2DEuclideanDistance(potongBobotTrain, potongBobotUji, jumlahPose);
                
                % menyimpan citra yang sedang diuji
                yangDiuji = data_testing{jOrangTesting, jPoseTesting};
                % menyimpa citra yang dengan jarak terdekat
                hasilUji = data_training{outputP.orangKe, outputP.poseKe};
                
                % menyimpan variabel yangDiuji dan hasilUji kedalam array
                % hasil
                hasil = [yangDiuji hasilUji];
                % menambahkan array hasil kedalam variabel kumpulanHasil
                kumpulanHasil = [kumpulanHasil; hasil];
                % menggerakkan progres pada waitbar
                waitbar(counter/(jumlahOrangTesting*jumlahPoseTesting));
                
                % jika citra yang diuji berada pada baris yang sama dengan citra training
                if outputP.orangKe == jOrangTesting
                    % variabel benar ditambah 1
                    benar = benar + 1;
                    % variabel benarSetiapKelas ditambah 1
                    benarSetiapKelas = benarSetiapKelas + 1;
                    % menyimpan indeks baris dan kolom dari citra testing
                    % serta angka 1 didalam variabel
                    % detilKesimpulanTiapKelas
                    detilKesimpulanTiapKelas = [detilKesimpulanTiapKelas; jOrangTesting jPoseTesting 1];
                % jika citra yang diuji berada pada baris yang tidak sama dengan citra training
                else
                    % variabel salah ditambah 1
                    salah = salah + 1;
                    % variabel salahSetiapKelas ditambah 1
                    salahSetiapKelas = salahSetiapKelas + 1;
                    % menyimpan indeks baris dan kolom dari citra testing
                    % serta angka 0 didalam variabel
                    % detilKesimpulanTiapKelas
                    detilKesimpulanTiapKelas = [detilKesimpulanTiapKelas; jOrangTesting jPoseTesting 0];
                end
            end
            % menambahkan array detilKesimpulan kedalam array variabel detilKesimpulan
            detilKesimpulan = [detilKesimpulan; detilKesimpulanTiapKelas];
            % mendefinisikan nama sheet
            namaSheet = ['jumlahfitur-' num2str(fitur)];
            % menyimpan indeks citra yang diuji, benarSetiapKelas dan
            % salahSetiapKelas
            hasilSementara = [jOrangTesting benarSetiapKelas salahSetiapKelas];
            % menambahkan array hasilSementara kedalam array hasilAkhir
            hasilAkhir = [hasilAkhir; hasilSementara];
        end
        %% simpan excel tiap hasil akhir
        % generate sheet untuk menampung array hasilAkhir
        xlswrite(namaFile, hasilAkhir, namaSheet);
        %% simpan excel tiap detail percobaan
        % generate sheet untuk menampung array detilKesimpulan
        xlswrite(namaFile, detilKesimpulan, ['Detail_percobaan_ke-' num2str(fitur)]);
        % menutup jendela waitbar
        close(h);
        % menghitung persentasi akurasi kecocokan citra
        prosentasiAkurasi = (benar/(benar+salah))*100;
        % menyimpan persentasi berdasarkan fitur dan iterasi kfold-nya
        tiapKfold(fitur-fiturAwal+1, iterKfold) = prosentasiAkurasi;
        % menyimpan indeks pose data training, jumlah fitur dan persentasi akurasinya
        expResult = [expResult; trainIdx fitur prosentasiAkurasi];        
    end
    %% simpan excel tiap detail percobaan
    % menyimpan array expResult kedalam array xValidation
    xValidation = [xValidation; expResult];
    % generate sheet untuk menampung array xValidation
    xlswrite(namaFile, xValidation, 'k-fold');
end

%% menampilkan bar
% menampilkan jendela figure
figure
% menampilkan diagram batang
bar(fiturAwal:fiturAkhir, tiapKfold, 'm');