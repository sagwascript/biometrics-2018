function convCell2Mat(dataCell)
[jumlahKelas, jumlahPose] = size(dataCell);
[hImage, wImage] = size(dataCell{1,1});
progress = waitbar(0, 'Proses');
jumlahImage = 0;
%dataMat = [];
for jKelas = 1:jumlahKelas
    for jPose = 1:jumlahPose
        jumlahImage = jumlahImage + 1;
        mat1DimDataTesting{jKelas, jPose} = reshape(double(dataCell{jKelas, jPose}), [1 hImage*wImage]);
        %mat1Dim = reshape(double(dataCell{jKelas, jPose}), [1 hImage*wImage]);
        %dataMat = [dataMat;mat1Dimx];
        waitbar(jumlahImage/(jumlahKelas*jumlahPose));
    end
end
close(progress);
save mat1DimDataTesting mat1DimDataTesting;
%save dataMat dataMat
