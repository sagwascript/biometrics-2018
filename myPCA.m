function hasil = myPCA(dataTraining)
dataTraining = double(dataTraining);
jumlahData = size(dataTraining, 1);
%% mencari mean
rata2 = mean(dataTraining, 1);
%% menghitung zero mean
zeroMean = dataTraining-repmat(rata2, jumlahData, 1);
%% menghitung covariance
kovarian = (1/(jumlahData-1))*zeroMean*transpose(zeroMean);
%% menghitung eigenvalue dan eigenvector dan mengurutkan secara decreasing eigenvaluenya diikuti dengan kolom eigenvector
[eigvector eigvalue] = svd(kovarian);
hasil.proyeksi = transpose(zeroMean) * eigvector;
hasil.bobot = dataTraining * hasil.proyeksi;