function hasil = myLDA(dataTraining)
jumlahPose = 3;
dataTraining = double(dataTraining);
jumlahData = size(dataTraining, 1);

%% mencari mean
rata2 = mean(dataTraining, 1);

%% menghitung zero mean
zeroMean = dataTraining-repmat(rata2, jumlahData, 1);

%% menghitung covariance
kovarian = (1/(jumlahData-1))*zeroMean*transpose(zeroMean);

%% menghitung eigenvalue dan eigenvector dan mengurutkan secara decreasing eigenvaluenya diikuti dengan kolom eigenvector
[eigvector eigvalue] = svd(kovarian);
proyeksiPCA = transpose(zeroMean) * eigvector;
bobotPCA = dataTraining * proyeksiPCA;

%% normalisasi data
proyeksiPCANormalisasi = [];
for iterasiBarisProyeksiPCA = 1:size(proyeksiPCA, 2)
   hitung = proyeksiPCA(iterasiBarisProyeksiPCA,:) .* 2;
   proyeksiPCANormalisasi = [proyeksiPCANormalisasi 1/(sum(hitung .* 0.5))];
end
proyeksiPCANormalisasi = repmat(proyeksiPCANormalisasi, length(proyeksiPCA), 1);
%proyeksiPCA = proyeksiPCA .* proyeksiPCANormalisasi;
inputLDA = zeroMean * proyeksiPCA;
[barisDataLDA kolomDataLDA] = size(inputLDA);

%% menghitung rata-rata tiap kelas
counter = 1;
counterKelas = 1;
matTiapKelas = [];
for iterasiKelas=1:barisDataLDA    
    matTiapKelas = [matTiapKelas; inputLDA(iterasiKelas, :)];
    if counter == jumlahPose
        dataTiapKelas{counterKelas, 1} = matTiapKelas;
        matTiapKelas = [];
        counter = 0;
        counterKelas = counterKelas + 1;
    end
    counter = counter + 1;
end

meanTiapKelas = [];

for iterasiTiapKelas = 1:length(dataTiapKelas)
    meanTiapKelas = [meanTiapKelas; mean(dataTiapKelas{iterasiTiapKelas, 1})];
end

%% menghitung rata-rata data set
meanSemuaDataTraining = mean(inputLDA);

%% menghitung SB
SB = 0;
for iterasiMeanKelas = 1:size(meanTiapKelas, 1)
    rumus = meanTiapKelas(iterasiMeanKelas, :)-meanSemuaDataTraining(1,:);
    hitung = transpose(rumus) * rumus;
    % IMPORTANT NOTE: SB otomatis ditransformasikan menjadi matriks 0
    % dengan ukuran jumlahkelas x jumlahpose
    SB = SB + jumlahPose * hitung;
end

%% menghitung SW
SW = 0;
for iterasiKelas = 1:size(meanTiapKelas, 1)
    for iterasiKolom = 1:length(inputLDA)
        rumus = inputLDA(iterasiKolom,:)-meanTiapKelas(iterasiKelas,:);
        hitung = transpose(rumus) * rumus;
        SW = SW + (1/jumlahPose-1) * hitung;
    end
end

%% menghitung eigenvector dan eigenvalue input LDA

[eigvectorLDA eigvalueLDA] = svd(SB*inv(SW));
eigvectorLDA = transpose(eigvectorLDA(:,1:length(eigvectorLDA)-1));
hasil.proyeksi = (eigvectorLDA * proyeksiPCA')';
hasil.bobot = dataTraining * hasil.proyeksi;