clear all
clc
load data_training
load mat1DimDataTesting
load dataMat
load data_testing

%orangKe = 10;
%poseKe = 1;
jumlahPose = size(data_training, 2);
namaFile = ['hasilexp-' num2str(jumlahPose) '.xls']
hasilAkhir = [];

%% proses PCA
outputPCA = myPCA2D(data_training);
[jumlahOrangTesting jumlahPoseTesting] = size(data_testing);
fiturAwal = 10;
fiturAkhir = 15;
kesimpulan = [];

%% proses uji 
for fitur = fiturAwal:fiturAkhir
    h = waitbar(0, 'Proses');
    counter = 0;
    hasilAkhir = [];
    benar = 0;
    salah = 0;
    
    [nkelas npose] = size(outputPCA.bobot);
    for iterKelas = 1:nkelas
       for iterPose = 1:npose
           potongBobotTrain{iterKelas, iterPose} = outputPCA.bobot{iterKelas, iterPose}(:,1:fitur);
       end
    end
    
    detilKesimpulan = [];
    for jOrangTesting = 1:jumlahOrangTesting
        kumpulanHasil = [];
        benarSetiapKelas = 0;
        salahSetiapKelas = 0;
        detilKesimpulanTiapKelas = [];
        for jPoseTesting = 1:jumlahPoseTesting
            counter = counter + 1;
            potongBobotUji = double(data_testing{jOrangTesting, jPoseTesting}) * outputPCA.proyeksi(:,1:fitur);
             
            outputP = my2DEuclideanDistance(potongBobotTrain, potongBobotUji, jumlahPose);
           
            yangDiuji = data_testing{jOrangTesting, jPoseTesting};
            hasilUji = data_training{outputP.orangKe, outputP.poseKe};
            
            hasil = [yangDiuji hasilUji];
            kumpulanHasil = [kumpulanHasil; hasil];
            waitbar(counter/(jumlahOrangTesting*jumlahPoseTesting));
            if outputP.orangKe == jOrangTesting
                benar = benar + 1;
                benarSetiapKelas = benarSetiapKelas + 1;
                detilKesimpulanTiapKelas = [detilKesimpulanTiapKelas; jOrangTesting jPoseTesting 1];
            else
                salah = salah + 1;
                salahSetiapKelas = salahSetiapKelas + 1;
                detilKesimpulanTiapKelas = [detilKesimpulanTiapKelas; jOrangTesting jPoseTesting 0];
            end
        end
        detilKesimpulan = [detilKesimpulan; detilKesimpulanTiapKelas];
        namaSheet = ['jumlahfitur-' num2str(fitur)];
        hasilSementara = [jOrangTesting benarSetiapKelas salahSetiapKelas];
        hasilAkhir = [hasilAkhir; hasilSementara];
        namaFileImg = ['2DPCA orangke-' num2str(jOrangTesting) '.jpg'];
        imwrite(kumpulanHasil, namaFileImg);
    end
    %% simpan excel tiap hasil akhir
    xlswrite(namaFile, hasilAkhir, namaSheet);
    %% simpan excel tiap detail percobaan
    xlswrite(namaFile, detilKesimpulan, ['Detail_percobaan_ke-' num2str(fitur)]);
    
    sementara = [fitur benar salah];
    hasilAkhir = [hasilAkhir; sementara];
    kesimpulan = [kesimpulan; sementara];
    close(h);
end
prosentasiAkurasi = (benar/(benar+salah))*100;
save kesimpulan kesimpulan;
xlswrite(namaFile, kesimpulan, 'Kesimpulan');
%% menampilkan bar
figure
bar(kesimpulan(:, 1), kesimpulan(:, [2 3]));