function outputP = myManhattan(bobotTraining, bobotTesting, jumlahPose)
jumlahBaris = size(bobotTraining, 1);
for jBaris = 1:jumlahBaris
   jarak = abs(bobotTesting-bobotTraining(jBaris, :));
   jaraknya(jBaris) = sum(jarak);
end
[nilaiMin posisiMin] = min(jaraknya);
outputP.orangKe = ceil(posisiMin/jumlahPose);
outputP.poseKe = mod(posisiMin, jumlahPose);
if outputP.poseKe == 0
    outputP.poseKe = jumlahPose;
end

