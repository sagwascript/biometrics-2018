function result = diagonalize(image)
% konversi nilai citra ke tipe double
A = im2double(image);
% mendapatkan ukuran citra
[m, n] = size(A);
% mendefinisikan array 0 seukuran citra
result = zeros(m, n);

% jika nilai m lebih kecil atau sama dengan dari nilai n
if m <= n
    % mengubah citra A menjadi 2 citra yang berjajar kesamping
    A = cat(2, A, A);
    % mengambil ukuran citra A yang sudah berubah
    [newM newN] = size(A);
    % perulangan pada tiap baris citra
    for baris = 1:m
        % mengisikan tiap baris array result berdasarkan iterasi baris dari citra A
        % hingga ukuran citra asli
        result(baris,:) = A(baris, baris:baris+n-1);
    end
% jika nilai m lebih besar atau sama dengan dari nilai n
elseif m >= n
    % mengubah citra A menjadi 2 citra yang berjajar kebawah
    A = cat(1, A, A);
    % mengambil ukuran citra A yang sudah berubah
    [newM newN] = size(A);
    % perulangan pada tiap kolom citra
    for kolom = 1:n
        % mengisikan tiap kolom array result berdasarkan iterasi kolom dari perulangan
        % hingga ukuran citra asli
        result(:,kolom) = A(kolom:kolom+m-1, kolom);
    end
end
