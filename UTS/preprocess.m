function preprocess(dataMat)
    % mendapatkan banyak baris dan kolom dari dataset
    [nrow ncol] = size(dataMat);
    % perulangan pada setiap baris dan kolom dataset
    for i=1:nrow
       for j=1:ncol
          % menyimpan citra yang sudah dilakukan proses diagonalisasi
          % kedalam variabel diaDataMat
          diaDataMat{i, j} = diagonalize(dataMat{i, j});
       end
    end
    % menyimpan variabel diaDataMat ke bentuk file mat
    save diaDataMat diaDataMat
end
