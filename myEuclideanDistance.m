function outputP = myEuclideanDistance(bobotTraining, bobotTesting, jumlahPose)
jumlahBaris = size(bobotTraining, 1);
for jBaris = 1:jumlahBaris
   jarak = (bobotTesting-bobotTraining(jBaris, :)).^2;
   jaraknya(jBaris) = sqrt(sum(jarak));
end
[nilaiMin posisiMin] = min(jaraknya);
outputP.orangKe = ceil(posisiMin/jumlahPose);
outputP.poseKe = mod(posisiMin, jumlahPose);
if outputP.poseKe == 0
    outputP.poseKe = jumlahPose;
end